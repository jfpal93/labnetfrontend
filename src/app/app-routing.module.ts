import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/public/login/login.component';
import { P404Component } from './components/site/errors/p404/p404.component';
import { AuthGuard } from './helpers/auth.guard';

const routes: Routes = [
  { path: 'home', component: LoginComponent},
  { path: '404', 
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path:'admin',
    canActivate:[AuthGuard],
    data:{role:'admin'},
    loadChildren:()=>import('./components/site/admin/admin.module').then(m=>m.AdminModule)
  },
  {
    path:'student',
    canActivate:[AuthGuard],
    data:{role:'student'},
    loadChildren:()=>import('./components/site/student/student.module').then(m=>m.StudentModule)
  },
  {
    path:'teachers',
    canActivate:[AuthGuard],
    data:{role:'teachers'},
    loadChildren:()=>import('./components/site/teachers/teachers.module').then(m=>m.TeachersModule)
  },  
  { path: '',
  
  redirectTo:'/admin',
  
  pathMatch:'full'
  
  },   
  { path: '',
  
  redirectTo:'/student',
  
  pathMatch:'full'
  
  },   
  { path: '',
  
  redirectTo:'/teachers',
  
  pathMatch:'full'
  
  } , 
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
