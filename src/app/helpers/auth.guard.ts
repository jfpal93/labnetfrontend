import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

import {JWTTokenService} from './../services/jwttoken.service';
import { RoleService } from './../services/role.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private jwt: JWTTokenService, 
    private roleService: RoleService, 
    private router: Router,
    private auth:AuthService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.jwt.isValid() && !this.jwt.isTokenExpired()) {
        if(this.roleService.getRole() !== null && 
        next.data.role.indexOf(this.roleService.getRole()) === -1){
          
          this.router.navigate(['/'+this.roleService.getRole()]);
          return false;
        }
        return true;
      } else {
        this.router.navigate(['/home']).then(
          ()=>{
            this.auth.clear();
            window.location.reload();
          }
        );
        return false;
      }
  }
  
}
