import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class ProfesoresService {

  constructor(private req:RequestService) { }

  get(){
    return this.req.get('users/profesores');
  }

  save(data){
    return this.req.post('teacher/create',data);
  }

  edit(data,id){
    return this.req.post('teacher/updateProf/'+id,data);
  }

  delete(id,data){
    return this.req.post('teacher/delete/'+id,data);
  }

  createNbind(id,data){
    return this.req.post('users/profesores/createNbind/'+id,data);
  }

  unbind(id){
    return this.req.post('users/profesores/unbindProf/'+id,null);
  }

  attachProfToCourse(data,id){
    return this.req.post('teahcer/attachToCourse/'+id,data);
  }

  getCourses(id){
    return this.req.get('users/teacher/courses/'+id);
  }
}
