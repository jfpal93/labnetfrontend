import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private BASE_URL = environment.BASE_URL;

  constructor(private http: HttpClient) { }

  post(url,data){
    return this.http.post(`${this.BASE_URL}/${url}`,data);
  }

  get(url){
    return this.http.get(`${this.BASE_URL}/${url}`);
  }

  put(url,data,id){
    return this.http.put(`${this.BASE_URL}/${url}/${id}`,data);
  }

  delete(url,id){
    return this.http.delete(`${this.BASE_URL}/${url}/${id}`);
  }

  postWithoutData(url){
    return this.http.post(`${this.BASE_URL}/${url}`,null);
  }
}
