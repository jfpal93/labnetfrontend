import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class LaboratoriosService {

  constructor(private req:RequestService) { }

  save(data){
    return this.req.post('laboratorios',data);
  }

  edit(data,id){
    return this.req.put('laboratorios',data,id);
  }

  lab(){
    return this.req.get('lab')
  }

  saveActividad(data){
    return this.req.post('actividades',data);
  }

  editActividad(data,id){
    return this.req.put('actividades',data,id);
  }

  iniciarLaboratorio(data){
    return this.req.post('users/students/startLab',data);
  }

  continuarLaboratorio(data){
    return this.req.post('users/students/continueLab',data);
  }

  addc7200(data){
    return this.req.post('users/students/addc7200',data);
  }

  getNodes(data){
    return this.req.get('users/students/nodes/'+data);
  }

  changeDevicePos(id,nodeId,data){
    return this.req.post('users/students/nodes/'+id+'/changePos/'+nodeId,data);
  }

  getLinks(data){
    return this.req.get('users/students/links/'+data);
  }

  getLabData(id){
    return this.req.get('users/students/lab/'+id);
  }

  linkNodes(id,data){
    return this.req.post('users/students/link/'+id,data);
  }

  unlink(id,link){
    return this.req.post('users/students/linkDel/'+id+'/'+link,null);
  }

  turnOnNode(id,node){
    return this.req.post('users/students/lab/'+id+'/node/'+node+'/turnOn',null);
  }

  turnOffNode(id,node){
    return this.req.post('users/students/lab/'+id+'/node/'+node+'/turnOff',null);
  }
}
