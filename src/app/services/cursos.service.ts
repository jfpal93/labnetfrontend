import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  constructor(private req:RequestService) { }

  get(){
    return this.req.get('cursos');
  }

  save(data){
    return this.req.post('cursos',data);
  }

  edit(data,id){
    return this.req.put('cursos',data,id);
  }

  delete(id){
    return this.req.delete('cursos',id);
  }

  createNbind(id,data){
    return this.req.post('cursos/createNbind/'+id,data);
  }

  unbind(id){
    return this.req.post('cursos/unbindCiclo/'+id,null);
  }

  attachProfToCourse(data,id){
    return this.req.post('cursos/attachProf/'+id,data);
  }
}
