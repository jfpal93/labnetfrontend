import { Injectable } from '@angular/core';
import { CryptoService } from './crypto.service';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private crypt:CryptoService) { }

  getRole(){
    let user = this.crypt.decryptFoo();
    return user.user.roles[0].name;
  }

  // set(data) {
  //   localStorage.setItem('role', data);
  // }
}
