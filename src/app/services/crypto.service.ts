import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';  
import { LocalStorageService } from './local-storage.service';
import { LocalStorage } from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  private KEY="RgUkXp2s5v8y/B?E(H+MbQeThVmYq3t6w9z$C&F)J@NcRfUjXnZr4u7x!A%D*G-K";

  constructor(private localStorage:LocalStorageService,
    protected localStrg: LocalStorage) { }

  encrypt(string){

    let data =JSON.stringify(string);

    let enc=CryptoJS.AES.encrypt(data,this.KEY).toString();

    this.localStorage.set('foo',enc);
  }

  decrypt(string){
    var bytes  = CryptoJS.AES.decrypt(string, this.KEY);
    return bytes.toString(CryptoJS.enc.Utf8);
  }

  decryptFoo(){
    var foo=this.localStorage.get('foo');
    if(foo){
      return JSON.parse(this.decrypt(foo));
    }
    return null;
  }

  encryptOther(string){

    let data =JSON.stringify(string);

    let enc=CryptoJS.AES.encrypt(data,this.KEY).toString();

    this.localStorage.set('foo2',enc);

    // return this.localStorage.get('foo2')
  }

  decryptOther(){
    var foo=this.localStorage.get('foo2');
    if(foo){
      return JSON.parse(this.decrypt(foo));
    }
    return null;
  }
}
