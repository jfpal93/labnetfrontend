import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { environment } from 'src/environments/environment';
import { CryptoService } from './crypto.service';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class JWTTokenService {

  private BASE_URL = environment.BASE_URL;

  jwtToken: string;
  decodedToken: { [key: string]: string };

  constructor(private storage:LocalStorageService,
    private crypto:CryptoService) { }

  setToken(token: string) {
    if (token) {
      this.jwtToken = token;
    }
  }

  decodeToken() {    
    if (this.crypto.decryptFoo().access_token) {
      this.decodedToken = jwt_decode(this.crypto.decryptFoo().access_token);
    }
  }

  getDecodeToken(token) {
    return jwt_decode(token);
  }

  getUser() {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken.displayname : null;
  }

  getEmailId() {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken.email : null;
  }

  getExpiryTime() {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken.exp : null;
  }

  getToken(){
    var data =this.crypto.decryptFoo();
    if(data){
      return data.access_token;
    }
    return null;
  }

  isTokenExpired(): boolean {
    const expiryTime:any = this.getExpiryTime();    
    if (expiryTime) {
      return ((1000 * expiryTime) - (new Date()).getTime()) < 5000;
    } else {
      return false;
    }
  }

  isValid() {
    const token = this.getToken();
    if (token) {
      const payload = this.getDecodeToken(token);
      if (payload) {
        return (payload.iss === `${this.BASE_URL}/auth/login` ? true : false);
      }
    }
    return false;
  }
}
