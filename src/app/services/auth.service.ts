import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CryptoService } from './crypto.service';
import { JWTTokenService } from './jwttoken.service';
import { LocalStorageService } from './local-storage.service';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private crypt:CryptoService,
    private http:HttpClient,
    private localStorage:LocalStorageService,
    private jwt:JWTTokenService,
    private req:RequestService) { }

  login(data){
    return this.req.post('auth/login',data);
  }

  getName(){
    let user = this.crypt.decryptFoo();
    return user.user.name+" "+user.user.lastname ;
  }

  getID(){
    let user = this.crypt.decryptFoo();
    return user.user.id ;
  }

  logout(){
    return this.req.postWithoutData('auth/logout');
  }

  deleteFOO(){
    this.localStorage.remove('foo');
  }

  me(){
    return this.req.postWithoutData('auth/me');
  }

  clear(){
    this.localStorage.clear();
  }
  
}
