import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class EstudiantesService {

  constructor(private req:RequestService) { }

  get(){
    return this.req.get('users/students');
  }

  save(data){
    return this.req.post('student/create',data);
  }

  edit(data,id){
    return this.req.post('student/updateStudent/'+id,data);
  }

  delete(id,data){
    return this.req.post('student/delete/'+id,data);
  }

  createNbind(id,data){
    return this.req.post('users/profesores/createNbind/'+id,data);
  }

  unbind(id,data){
    return this.req.post('users/students/unbind/'+id,data);
  }

  attachStudentToCourse(data,id){
    return this.req.post('student/attachToCourse/'+id,data);
  }

  getCourses(id){
    return this.req.get('users/student/courses/'+id);
  }
}
