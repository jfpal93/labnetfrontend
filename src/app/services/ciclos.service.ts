import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class CiclosService {

  constructor(private req:RequestService) { }

  get(){
    return this.req.get('ciclos');
  }

  save(data){
    return this.req.post('ciclos',data);
  }

  edit(data,id){
    return this.req.put('ciclos',data,id);
  }

  delete(id){
    return this.req.delete('ciclos',id);
  }

  attachCourseToCiclo(data,id){
    return this.req.post('ciclos/attachCourse/'+id,data);
  }

  attachNewCourseToCiclo(data,id){
    return this.req.put('cursos/createNbind',data,id);
  }

}
