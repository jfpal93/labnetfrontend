import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CryptoService } from 'src/app/services/crypto.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-left-window',
  templateUrl: './left-window.component.html',
  styleUrls: ['./left-window.component.scss']
})
export class LeftWindowComponent implements OnInit {

  lab;

  constructor(
    private location:Location,
    private crypt:CryptoService,
    private router:Router,
    private localstorage:LocalStorageService,
    ) { }

  ngOnInit(): void {
    
    
  }
  

  isCanva() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if(titlee.charAt(0) === '#'){
        titlee = titlee.slice( 1 );
    }
    if( titlee === '/student/canvaLab' ) {
        return true;
    }
    else {
        return false;
    }
  }

  

}
