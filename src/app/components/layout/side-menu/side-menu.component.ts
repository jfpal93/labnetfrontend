import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleService } from 'src/app/services/role.service';

declare interface role {
  name:string,
  route:RouteInfo[]
}

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const adminRoute: RouteInfo[] = [
  { path: '/admin/', title: 'Dashboard',  icon: 'lnr lnr-home', class: '' },
  { path: '/admin/ciclos', title: 'Ciclos',  icon:'lnr lnr-list', class: '' },
  { path: '/admin/cursos', title: 'Cursos',  icon:'lnr lnr-layers', class: '' },
  { path: '/admin/profesores', title: 'Profesores',  icon:'lnr lnr-graduation-hat', class: '' },
  { path: '/admin/estudiantes', title: 'Estudiantes',  icon:'lnr lnr-users', class: '' },
  // { path: '/admin/perfil', title: 'Perfil',  icon:'lnr lnr-user', class: '' },
];

export const teachersRoute: RouteInfo[] = [
  { path: '/teachers/', title: 'Dashboard',  icon: 'lnr nr-home', class: '' },
  { path: '/teachers/cursos', title: 'Cursos',  icon:'lnr lnr-layers', class: '' },
  { path: '/teachers/laboratorios', title: 'Laboratorios',  icon:'lnr lnr-rocket', class: '' },
  { path: '/teachers/calificaciones', title: 'Calificaciones',  icon:'lnr lnr-star', class: '' },
  // { path: '/teachers/perfil', title: 'Perfil',  icon:'lnr lnr-user', class: '' },
];

export const studentRoute: RouteInfo[] = [
  { path: '/student/', title: 'Dashboard',  icon: 'lnr lnr-home', class: '' },
  { path: '/student/laboratorios', title: 'Laboratorios',  icon:'lnr lnr-rocket', class: '' },
  { path: '/student/calificaciones', title: 'Calificacioens',  icon:'lnr lnr-star', class: '' },
  // { path: '/student/perfil', title: 'Perfil',  icon:'lnr lnr-user', class: '' },
];

export const roles= {
  'admin':{ 
    
    route: adminRoute
  },
  'teachers':{ 
    route: teachersRoute
  },
  'student':{ 
    route: studentRoute
  }
};

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  

  constructor(
    private roles:RoleService,
    private router: Router,
    private location:Location
    ) { }

  ngOnInit(): void {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    
  }

  showMenuByRole(){
    switch(this.roles.getRole()){
      case 'admin':
        return roles.admin.route;
        break;
      case 'teachers':
        return roles.teachers.route;
        break;
      case 'student':
        return roles.student.route;
        break;
    }
  }

  isCanva() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if(titlee.charAt(0) === '#'){
        titlee = titlee.slice( 1 );
    }
    if( titlee === '/student/canvaLab' ) {
        return true;
    }
    else {
        return false;
    }
  }

}
