import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-right-window',
  templateUrl: './right-window.component.html',
  styleUrls: ['./right-window.component.scss']
})
export class RightWindowComponent implements OnInit {

  constructor(private location:Location) { }

  ngOnInit(): void {
  }

  isCanva() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if(titlee.charAt(0) === '#'){
        titlee = titlee.slice( 1 );
    }
    if( titlee === '/student/canvaLab' ) {
        return true;
    }
    else {
        return false;
    }
  }

}
