import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  name:string="";

  constructor(private auth:AuthService,
    private router:Router,
    private location:Location) {
    this.name=this.auth.getName();
   }

  ngOnInit(): void {
  }

  logout(){
    this.handleResponse();
    this.auth.logout().subscribe(
      (data:any)=>{
        this.router.navigateByUrl('/home');
      }
    );
  }

  handleResponse(){
    this.auth.clear();
    
  }

  isCanva() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if(titlee.charAt(0) === '#'){
        titlee = titlee.slice( 1 );
    }
    if( titlee === '/student/canvaLab' ) {
        return true;
    }
    else {
        return false;
    }
  }

}
