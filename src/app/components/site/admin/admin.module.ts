import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { CiclosComponent } from './ciclos/ciclos.component';
import { CursosComponent } from './cursos/cursos.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EstudiantesComponent } from './estudiantes/estudiantes.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ProfesoresComponent } from './profesores/profesores.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import locale from '@angular/common/locales/es-Ec';
import { registerLocaleData } from '@angular/common';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

registerLocaleData(locale);
@NgModule({
  declarations: [
    CiclosComponent,
    CursosComponent,
    DashboardComponent,
    EstudiantesComponent,
    PerfilComponent,
    ProfesoresComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AdminRoutingModule,
    NgbModule,
    NgxPaginationModule,
    Ng2SearchPipeModule
  ]
})
export class AdminModule { }
