import { Component, OnInit } from '@angular/core';
import { EstudiantesService } from 'src/app/services/estudiantes.service';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styleUrls: ['./estudiantes.component.scss']
})
export class EstudiantesComponent implements OnInit {

  public ccls:any[];
  public estudiantes:any[];

  search:string;
  search2:string;
  p;
  p2;

  public curso={
    id:null,
    codigo:null,
    estudiantes:[]
  };

  public estForm={
    id:null,
    name:null,
    lastname:null,
    email:null,
    telefono:null
  }

  public addStudentForm={
    studentId:null,
    id:null
  }

  editLayout:boolean=false;
  showLayout:boolean=false;
  addLayout:boolean=false;

  showStudentListForm:boolean=false;

  loadingSaving:boolean=false;

  loadingSaving2:boolean=false;

  constructor(private studentService:EstudiantesService) { }

  ngOnInit(): void {
    this.getCiclos();
  }

  getCiclos(){
    this.studentService.get().subscribe(
      (data:any)=>{
        this.ccls=data.ciclos;
        this.getEstudiantes(data.students);
      }
    )
  }

  getEstudiantes(data){
    this.estudiantes=data;
  }

  addEstudiante(cu){
    this.curso=cu;
    this.addStudentForm.id=this.curso.id;
    document.getElementById("openModalButton").click();
  }

  showEstudiante(e){
    this.showLayout=true;
    this.estForm=e;
    document.getElementById("openModalButton").click();
  }

  editEstudiante(e){
    this.editLayout=true;
    this.showLayout=false;
    this.estForm=e;
    document.getElementById("openModalButton").click();
  }

  close(){
    this.resetCurso();
    this.resetestorm();
    document.getElementById("closeModalButton").click();
  }

  deleteProf(p){
    var r = confirm("¿Está seguro que desea eliminar a : "+p.name+" "+p.lastname+" como estudiante?");    
    if(r){
      this.studentService.delete(p.id,p).subscribe(
        (data:any)=>{
          this.ccls=data.ciclos;
          this.getEstudiantes(data.students);
        }
      )
    }
  }

  add(){
    this.editLayout=false;
    this.showLayout=false;
    this.addLayout=true;
    document.getElementById("openModalButton").click();
  }

  resetestorm(){
    this.estForm={
      id:null,
      name:null,
      lastname:null,
      email:null,
      telefono:null
    }
  }

  resetCurso(){
    this.curso={
      id:null,
      codigo:null,
      estudiantes:[]
    };
  }

  submitProf(){
    if(this.estForm.id){
      this.loadingSaving=true;
      this.studentService.edit(this.estForm,this.estForm.id).subscribe(
        (data:any)=>{
          this.loadingSaving=false;
          this.ccls=data.ciclos;
          this.getEstudiantes(data.students);
          this.close();
          this.resetestorm();
          this.resetCurso();
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 
    }
    else{
      this.loadingSaving=true;
      this.studentService.save(this.estForm).subscribe(
        (data:any)=>{
          this.loadingSaving=false;
          this.ccls=data.ciclos;
          this.getEstudiantes(data.students);
          this.close();
          this.resetestorm();
          this.resetCurso();
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 

    }
  }

  addStudent(){
    this.showStudentListForm=true;
    this.addStudentForm.studentId=null;
  }

  attachStudentToCourse(){
    this.loadingSaving2=true;
    // this.addCourseForm.id=this.form.id;
    this.studentService.attachStudentToCourse(this.addStudentForm,this.addStudentForm.id).subscribe(
      (data:any)=>{
        this.ccls=data.ciclos;
        this.getEstudiantes(data.students);
        this.resetestorm();
        this.resetCurso();
        this.curso=this.getCurrentCurso(this.addStudentForm.id);
        this.loadingSaving2=false;
        this.showStudentListForm=false;
      }
    )
  }

  getCurrentCurso(cursoID){
    let curso=null;
    for(let c of this.ccls){
      if(this.curso['ciclo_id']==c.id){
        for(let cu of c.cursos){
          if(cu.id==cursoID){
            curso = cu;
          }
        }
      }
    }
    return curso;
  }

  evt(evnt){
    if(evnt == "new"){
      // this.showCourseListForm=false;
      // this.showNewCourseForm=true;
      // this.newCourseForm.id=this.form.id
    }
  }

  detachStudentCourse(student){
    var r = confirm("¿Está seguro que desea eliminar a : "+student.name+" "+student.lastname+" como estudiante de este curso?");    
    if(r){
      this.studentService.unbind(student.pivot.curso_id,student).subscribe(
        (data:any)=>{
          this.ccls=data.ciclos;
          this.getEstudiantes(data.students);
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 
    }
  }
  
}
