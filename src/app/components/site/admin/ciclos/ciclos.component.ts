import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { CiclosService } from './../../../../services/ciclos.service';
import { CursosService } from './../../../../services/cursos.service';

declare var $: any;
@Component({
  selector: 'app-ciclos',
  templateUrl: './ciclos.component.html',
  styleUrls: ['./ciclos.component.scss']
})
export class CiclosComponent implements OnInit {

  @ViewChild('courses') div: ElementRef;

  readonly DELIMITER = '-';

  public form = {
    id:null,
    termino: null,
    ano_lectivo: null,
    descripcion: null,
    fechaIncio: null,
    fechaFin: null,
    activo:true
  };

  showCourse;
  filteredCourses;

  public addCourseForm={
    courseid:null,
    id:null
  }

  public newCourseForm={
    id:null,
    codigo:null,
    descripcion:null
    
  }

  editLayout:boolean=false;
  showLayout:boolean=false;
  loadingSaving:boolean=false;

  showCourseListForm:boolean=false;
  showNewCourseForm:boolean=false;

  public ccls:any[];
  public cursos:any[];

  search:string;
  p;
  p2;

  constructor(
    private ciclos:CiclosService,
    private cursosservice:CursosService
    ) { 
    
  }

  ngOnInit(): void {
    this.getCiclos();
  }

  getCiclos(){
    this.ciclos.get().subscribe(
      (data:any)=>{
        this.ccls=data.ciclos;
        this.cursos=data.cursos;
      }
    )
  }

  saveCiclos(){
    this.loadingSaving=true;
    if(!this.form.id){
      this.ciclos.save(this.form).subscribe(
        (data:any)=>{
          this.ccls=data;
          this.close();
          this.loadingSaving=false;
        }
      )
    }
    else{
      this.ciclos.edit(this.form,this.form.id).subscribe(
        (data:any)=>{
          this.ccls=data;
          this.close();
          this.loadingSaving=false;
        }
      );
    }
  }

  edit(c){
    this.editLayout=true;
    var splt1 = this.split(c.fechaIncio," ");
    var splt2 = this.split(c.fechaFin," ");
    
    c.fechaIncio=splt1[0];
    c.fechaFin=splt2[0];
    
    this.form=c;
    document.getElementById("openModalButton").click();
  }

  resetForm(){
    this.form = {
      id:null,
      termino: null,
      ano_lectivo: null,
      descripcion: null,
      fechaIncio: null,
      fechaFin: null,
      activo:true
    };
  }

  add(){
    this.resetForm();
    this.editLayout=false;
    this.showLayout=false;
    document.getElementById("openModalButton").click();
  }

  show(c){
    this.showLayout=true;

    var splt1 = this.split(c.fechaIncio," ");
    var splt2 = this.split(c.fechaFin," ");
    
    c.fechaIncio=splt1[0];
    c.fechaFin=splt2[0];

    this.form=c;
    this.showCourse=c.cursos;
    this.filteredCourses=this.cursos.filter(this.comparer(c.cursos));    
    document.getElementById("openModalButton").click();
  }

  comparer(otherArray){
    return function(current){
      return otherArray.filter(function(other){
        return other.ciclo_id == current.ciclo_id
      }).length == 0;
    }
  }

  close(){
    document.getElementById("closeModalButton").click();
    this.editLayout=false;
    this.showLayout=false;
    this.showCourseListForm=false;
    this.showNewCourseForm=false;
    this.resetForm();
  }

  returnDate(date){
    return new Date(date);
  }

  split(string,splt){
    return string.split(splt);
  }

  delete(c){
    var r = confirm("¿Está seguro que desea eliminar: "+c.termino+"-"+c.ano_lectivo);    
    if (r) {
      this.form=c;
      this.ciclos.delete(this.form.id).subscribe(
        (data:any)=>{
          this.ccls=data;
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
          
        }
      ); 
    } 
    this.form=c;
  }

  addCourse(){
    this.showCourseListForm=true;
    this.addCourseForm.courseid=null;
  }
  
  evt(evnt){
    if(evnt == "new"){
      this.showCourseListForm=false;
      this.showNewCourseForm=true;
      this.newCourseForm.id=this.form.id
    }
  }

  closeCourseListForm(){
    this.showCourseListForm=false;
  }

  closeNewCourseForm(){
    this.showNewCourseForm=false;
  }

  attachCourseToCiclo(){
    
    this.addCourseForm.id=this.form.id;
    this.ciclos.attachCourseToCiclo(this.addCourseForm,this.addCourseForm.id).subscribe(
      (data:any)=>{
        this.ccls=data.ciclos;
        this.cursos=data.cursos;
        this.showCourse=this.findElement('id',this.form.id).cursos;
        this.filteredCourses=this.cursos.filter(this.comparer(this.showCourse));    
        this.resetaddCourseForm();
        this.showCourseListForm=false;
      }
    )
  }

  resetaddCourseForm(){
    this.addCourseForm={
      courseid:null,
      id:null
    }
  }

  newCourseToCiclo(){
    this.cursosservice.createNbind(this.newCourseForm.id,this.newCourseForm).subscribe(
      (data:any)=>{
        this.ccls=data.ciclos;
        this.cursos=data.cursos;
        
        this.showNewCourseForm=false;
        
        this.showCourse=this.findElement('id',this.newCourseForm.id).cursos;
        this.filteredCourses=this.cursos.filter(this.comparer(this.showCourse));    
        this.resetNewcourseForm();
      },(errer:any)=>{
        alert("Hubo un error, intente más tarde...")
      }
    ); 

  }

  resetNewcourseForm(){
    this.newCourseForm={
      id:null,
      codigo:null,
      descripcion:null
    }
  }

  findElement(propName, propValue) {
    for (var i=0; i < this.ccls.length; i++)
      if (this.ccls[i][propName] == propValue)
        return this.ccls[i];
  }

  unbindCiclo(c){
    this.cursosservice.unbind(c.id).subscribe(
      (data:any)=>{
        this.ccls=data.ciclos;
        this.cursos=data.cursos;
                
        this.showCourse=this.findElement('id',c.ciclo_id).cursos;
        this.filteredCourses=this.cursos.filter(this.comparer(this.showCourse));    
      },(errer:any)=>{
        alert("Hubo un error, intente más tarde...")
      }
    ); 
  }

  closeShowCourseListForm(){
    this.showCourseListForm=false;
  }

}
