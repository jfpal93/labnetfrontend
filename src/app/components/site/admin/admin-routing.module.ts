import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CiclosComponent } from './ciclos/ciclos.component';
import { CursosComponent } from './cursos/cursos.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EstudiantesComponent } from './estudiantes/estudiantes.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ProfesoresComponent } from './profesores/profesores.component';


const routes: Routes = [
  {
    path:'',
    children:
    [
      {
        path: 'ciclos', 
        data:{role:'admin'},
        component: CiclosComponent 
      },
      {
        path: 'cursos', 
        data:{role:'admin'},
        component: CursosComponent
      },
      {
        path: 'estudiantes', 
        data:{role:'admin'},
        component: EstudiantesComponent 
      },
      {
        path: 'perfil', 
        data:{role:'admin'},
        component: PerfilComponent 
      },
      {
        path: 'profesores', 
        data:{role:'admin'},
        component: ProfesoresComponent 
      },
      {
        path: '', redirectTo: '', pathMatch: 'full',
        component: DashboardComponent
      },
      // { path: '**', component:  Client404Component}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
