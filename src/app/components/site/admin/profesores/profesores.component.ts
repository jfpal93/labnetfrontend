import { Component, OnInit } from '@angular/core';
import { ProfesoresService } from 'src/app/services/profesores.service';

@Component({
  selector: 'app-profesores',
  templateUrl: './profesores.component.html',
  styleUrls: ['./profesores.component.scss']
})
export class ProfesoresComponent implements OnInit {

  public ccls:any[];
  public profesores:any[];

  search:string;
  search2:string;
  p;
  p2;

  public addProfesorForm={
    id:null,
    profId:null
  }

  public newProfesorForm={
    id:null,
    name:null,
    lastname:null,
    email:null,
    telefono:null
  }

  public profForm={
    id:null,
    name:null,
    lastname:null,
    email:null,
    telefono:null
  }
  editLayout:boolean=false;
  showLayout:boolean=false;
  addLayout:boolean=false;

  curso;

  loadingSaving:boolean=false;
  loadingSaving2:boolean=false;

  constructor(
    private profService:ProfesoresService
    ) { }

  ngOnInit(): void {
    this.getCiclos();
  }

  getCiclos(){
    this.profService.get().subscribe(
      (data:any)=>{
        this.ccls=data.ciclos;
        this.getProfesores(data.profesores);
      }
    )
  }

  getProfesores(data){
    this.profesores=data;
  }

  unbindprofesores(cursoId,nombre,apellido){
    var r = confirm("¿Está seguro que desea eliminar a : "+nombre+" "+apellido+" como profesor/a de este curso?");    
    if(r){
      this.profService.unbind(cursoId).subscribe(
        (data:any)=>{
          this.getCiclos();
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 
    }
  }

  close(){
    document.getElementById("closeModalButton").click();
  }

  openModal(curso){
    this.addProfesorForm.id=curso.id;
    this.newProfesorForm.id=curso.id;
    this.curso=curso;
    this.editLayout=false;
    this.showLayout=false;
    this.addLayout=false;
    document.getElementById("openModalButton").click();
  }

  attachProfToCourse(){
    this.loadingSaving=true;
    this.profService.attachProfToCourse(this.addProfesorForm,this.addProfesorForm.id).subscribe(
      (data:any)=>{
        this.ccls=data.ciclos;
        this.getProfesores(data.profesores);
        this.close();
        this.resetaddProfForm();
        this.loadingSaving=false;
      }
    )
  }

  bindProfesor(){

  }

  resetaddProfForm(){
    this.addProfesorForm={
      id:null,
      profId:null
    }
  }

  resetaddNewForm(){
    this.newProfesorForm={
      id:null,
      name:null,
      lastname:null,
      email:null,
      telefono:null
    }
  }

  resetNewForm(){
    this.profForm={
      id:null,
      name:null,
      lastname:null,
      email:null,
      telefono:null
    }
  }

  newprofesorToCurso(){
    this.loadingSaving2=true;
    this.profService.createNbind(this.newProfesorForm.id,this.newProfesorForm).subscribe(
      (data:any)=>{
        this.loadingSaving2=false;
        this.ccls=data.ciclos;
        this.getProfesores(data.profesores);
        this.close();
        this.resetaddProfForm();
        this.resetaddNewForm();
      },(errer:any)=>{
        alert("Hubo un error, intente más tarde...")
      }
    ); 
  }

  showProf(p){
    this.showLayout=true;
    this.profForm=p;
    
    document.getElementById("openModalButton").click();
  }

  editProf(p){
    this.editLayout=true;
    this.showLayout=false;
    this.profForm=p
    document.getElementById("openModalButton").click();
  }

  submitProf(){
    if(this.profForm.id){
      this.loadingSaving=true;
      this.profService.edit(this.profForm,this.profForm.id).subscribe(
        (data:any)=>{
          this.loadingSaving=false;
          this.ccls=data.ciclos;
          this.getProfesores(data.profesores);
          this.close();
          this.resetaddProfForm();
          this.resetaddNewForm();
          this.resetNewForm();
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 
    }
    else{
      this.loadingSaving=true;
      this.profService.save(this.profForm).subscribe(
        (data:any)=>{
          this.loadingSaving=false;
          this.ccls=data.ciclos;
          this.getProfesores(data.profesores);
          this.close();
          this.resetaddProfForm();
          this.resetaddNewForm();
          this.resetNewForm();
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 

    }
  }

  deleteProf(p){
    var r = confirm("¿Está seguro que desea eliminar a : "+p.name+" "+p.lastname+" como profesor/a?");    
    if(r){
      this.profService.delete(p.id,p).subscribe(
        (data:any)=>{
          this.loadingSaving=false;
          this.ccls=data.ciclos;
          this.getProfesores(data.profesores);
          this.close();
          this.resetaddProfForm();
          this.resetaddNewForm();
          this.resetNewForm();
        }
      )
    }
  }

  add(){
    this.editLayout=false;
    this.showLayout=false;
    this.addLayout=true;
    document.getElementById("openModalButton").click();
  }

}
