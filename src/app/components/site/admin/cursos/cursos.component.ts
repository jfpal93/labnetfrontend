import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CiclosService } from 'src/app/services/ciclos.service';
import { CursosService } from 'src/app/services/cursos.service';
import { ProfesoresService } from 'src/app/services/profesores.service';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {

  @ViewChild('courses') div: ElementRef;

  readonly DELIMITER = '-';

  public form = {
    id:null,
    codigo: null,
    descripcion: null,
    ciclo_id: null
  };

  showCourse;
  filteredProfesores;

  public addProfesorForm={
    id:null,
    profId:null
    
  }

  public newProfesorForm={
    id:null,
    name:null,
    lastname:null,
    email:null,
    telefono:null
  }

  editLayout:boolean=false;
  showLayout:boolean=false;
  loadingSaving:boolean=false;

  showProfListForm:boolean=false;
  showNewProfForm:boolean=false;

  public ccls:any[];
  public profesores:any[];
  public ciclos:any[];

  search:string;
  p;
  p2;

  constructor(
    private ciclosService:CiclosService,
    private cursosservice:CursosService,
    private profService:ProfesoresService
    ) { 
    
  }

  ngOnInit(): void {
    this.getCursos();
  }

  getCursos(){
    this.cursosservice.get().subscribe(
      (data:any)=>{
        this.ccls=data.cursos;
        this.profesores=data.profesores;
        this.ciclos=data.ciclos;
      }
    )
  }

  saveCursos(){
    this.loadingSaving=true;
    if(!this.form.id){
      this.cursosservice.save(this.form).subscribe(
        (data:any)=>{
          this.ccls=data.cursos;
          this.profesores=data.profesores;
          this.ciclos=data.ciclos;
          this.close();
          this.loadingSaving=false;
        }
      )
    }
    else{
      this.cursosservice.edit(this.form,this.form.id).subscribe(
        (data:any)=>{
          this.ccls=data.cursos;
          this.profesores=data.profesores;
          this.ciclos=data.ciclos;
          this.close();
          this.loadingSaving=false;
        }
      );
    }
  }

  edit(c){
    this.editLayout=true;
    
    this.form=c;
    document.getElementById("openModalButton").click();
  }

  resetForm(){
    this.form = {
      id:null,
      codigo: null,
      descripcion: null,
      ciclo_id: null
    };
  }

  add(){
    this.resetForm();
    this.editLayout=false;
    this.showLayout=false;
    document.getElementById("openModalButton").click();
  }

  show(c){
    this.showLayout=true;
    this.form=c;
    document.getElementById("openModalButton").click();
  }

  comparer(otherArray){
    return function(current){
      return otherArray.filter(function(other){
        return other.ciclo_id == current.ciclo_id
      }).length == 0;
    }
  }

  close(){
    document.getElementById("closeModalButton").click();
    this.editLayout=false;
    this.showLayout=false;
    this.showProfListForm=false;
    this.showNewProfForm=false;
    this.resetForm();
  }

  returnDate(date){
    return new Date(date);
  }

  split(string,splt){
    return string.split(splt);
  }

  delete(c){
    var r = confirm("¿Está seguro que desea eliminar: "+c.codigo);    
    if (r) {
      this.form=c;
      this.cursosservice.delete(this.form.id).subscribe(
        (data:any)=>{
          this.ccls=data.cursos;
        this.profesores=data.profesores;
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
          
        }
      ); 
    } 
    this.form=c;
  }

  addProfesor(){
    this.showProfListForm=true;
    this.addProfesorForm.profId=null;
  }
  
  evt(evnt){
    if(evnt == "new"){
      this.showProfListForm=false;
      this.showNewProfForm=true;
      this.newProfesorForm.id=this.form.id
    }
  }

  closeCourseListForm(){
    this.showProfListForm=false;
  }

  closeNewCourseForm(){
    this.showNewProfForm=false;
  }

  attachProfToCourse(){
    
    this.addProfesorForm.id=this.form.id;
    this.cursosservice.attachProfToCourse(this.addProfesorForm,this.addProfesorForm.id).subscribe(
      (data:any)=>{
        this.ccls=data.cursos;
        this.profesores=data.profesores;
        this.ciclos=data.ciclos;
        this.resetaddCourseForm();
        this.showProfListForm=false;
        this.form=this.getDataFromArrById(this.ccls,this.form.id);
      }
    )
  }

  getDataFromArrById(arr,id){
    for(let d of arr){
      if(d.id==id){
        return d;
      }
    }
  }

  resetaddCourseForm(){
    this.addProfesorForm={
      id:null,
      profId:null
    }
  }

  newprofesorToCurso(){
    this.profService.createNbind(this.newProfesorForm.id,this.newProfesorForm).subscribe(
      (data:any)=>{
        this.ccls=data.cursos;
        this.profesores=data.profesores;
        this.ciclos=data.ciclos;
        this.showNewProfForm=false;
        this.form=this.getDataFromArrById(this.ccls,this.newProfesorForm.id);
        this.resetNewProfesorForm();
      },(errer:any)=>{
        alert("Hubo un error, intente más tarde...")
      }
    ); 
  }

  resetNewProfesorForm(){
    this.newProfesorForm={
      id:null,
      name:null,
      lastname:null,
      email:null,
      telefono:null
    }
  }

  findElement(propName, propValue) {
    for (var i=0; i < this.ccls.length; i++)
      if (this.ccls[i][propName] == propValue)
        return this.ccls[i];
  }

  unbindprofesores(){
    var r = confirm("¿Está seguro que desea eliminar a : "+this.form["profesor"][0].name+" como profesor/a de este curso?");    
    if(r){
      this.profService.unbind(this.form.id).subscribe(
        (data:any)=>{
          this.ccls=data.cursos;
          this.profesores=data.profesores;
          this.ciclos=data.ciclos;
          this.form['profesor']=[];
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 
    }
  }

  closeShowCourseListForm(){
    this.showProfListForm=false;
  }

}
