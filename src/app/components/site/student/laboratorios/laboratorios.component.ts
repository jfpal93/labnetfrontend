import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/services/auth.service';
import { CryptoService } from 'src/app/services/crypto.service';
import { EstudiantesService } from 'src/app/services/estudiantes.service';
import { LaboratoriosService } from 'src/app/services/laboratorios.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ProfesoresService } from 'src/app/services/profesores.service';

@Component({
  selector: 'app-laboratorios',
  templateUrl: './laboratorios.component.html',
  styleUrls: ['./laboratorios.component.scss']
})
export class LaboratoriosComponent implements OnInit {

  

  public ccls:any[];

  showLayout:boolean=false;
  editLayout:boolean=false;
  addLayout:boolean=false;
  loadingSaving:boolean=false;

  loadingSaving2:boolean=false; 

  spinners = true;

  hourStep = 1;
  minuteStep = 30;

  public lab={
    id:null,
    nombre:null,
    descripcion:null,
    activo:null,
    calificacion:null,
    curso_id:null,
    user_id:null
  };

  public actividad={
    id:null,
    nombre:null,
    descripcion:null,
    puntaje:null,
    fecha_vencimiento:null,
    fecha_completo:null,
    laboratorio_id:null
  }

  model: NgbDateStruct;

  search:string;
  p;
  p2;

  constructor(
    // private calendar: NgbCalendar,
    private router:Router,
    private studentService:EstudiantesService,
    private auth:AuthService,
    private laboratoriosService:LaboratoriosService,
    private crypt:CryptoService
  ) { }

  ngOnInit(): void {
    this.getCursos();
    
  }

  getCursos(){
    
    this.studentService.getCourses(this.auth.getID()).subscribe(
      (data:any)=>{
        this.ccls=data;
      }
    );
  }

  addLab(cu){
    this.lab.curso_id=cu.id;
    this.lab.user_id=this.auth.getID();
    document.getElementById("openModalButton").click();
  }

  close(){
    document.getElementById("closeModalButton").click();
  }

  saveLab(){
    this.loadingSaving=true;
    if(!this.lab.id){
      this.laboratoriosService.save(this.lab).subscribe(
        (data:any)=>{
          this.ccls=data;
          this.close();
          this.loadingSaving=false;
          this.resetLab();
        }
      )
    }
    else{
      this.laboratoriosService.edit(this.lab,this.lab.id).subscribe(
        (data:any)=>{
          this.ccls=data;
          this.close();
          this.loadingSaving=false;
          this.resetLab();
        }
      );
    }
  }

  resetLab(){
    this.lab={
      id:null,
      nombre:null,
      descripcion:null,
      activo:null,
      calificacion:null,
      curso_id:null,
      user_id:null
    };
  }

  resetAct(){
    this.actividad={
      id:null,
      nombre:null,
      descripcion:null,
      puntaje:null,
      fecha_vencimiento:null,
      fecha_completo:null,
      laboratorio_id:null
    };
  }

  addActividadLab(e){    
    this.actividad.laboratorio_id=e.id;    
    document.getElementById("openActividadModalButton").click();
  }

  closeActividad(){
    document.getElementById("myActividadModal").click();
  }

  saveActividad(){
    this.loadingSaving=true;
    
    if(!this.actividad.id){
      this.laboratoriosService.saveActividad(this.actividad).subscribe(
        (data:any)=>{
          this.ccls=data;
          this.loadingSaving=false;
          this.resetAct();
          this.closeActividad();
        }
      )
    }
    else{
      this.laboratoriosService.editActividad(this.actividad,this.actividad.id).subscribe(
        (data:any)=>{
          this.ccls=data;
          this.loadingSaving=false;
          this.resetAct();
          this.closeActividad();
        }
      );
    }
  }

  iniciarLaboratorio(e){
    let lab={
      lab_id:e.id,
      curso_id:e.curso_id
    }
    this.laboratoriosService.iniciarLaboratorio(lab).subscribe(
      (data:any)=>{
        this.crypt.encryptOther(data);
        this.router.navigateByUrl('/student/canvaLab');

      }
    );
  }

  continuarLaboratorio(e){
    let lab={
      lab_id:e.id,
      curso_id:e.curso_id
    }
    
    this.laboratoriosService.continuarLaboratorio(lab).subscribe(
      (data:any)=>{
        this.crypt.encryptOther(data)
        this.router.navigateByUrl('/student/canvaLab');

      }
    );
  }

}
