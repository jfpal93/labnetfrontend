import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { CalificacionesComponent} from './calificaciones/calificaciones.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LaboratoriosComponent } from './laboratorios/laboratorios.component';
import { PerfilComponent } from './perfil/perfil.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CanvasLabComponent } from './canvas-lab/canvas-lab.component';
import { CanvasWhiteboardModule } from 'ng2-canvas-whiteboard';

@NgModule({
  declarations: [
    CalificacionesComponent,
    DashboardComponent,
    LaboratoriosComponent,
    PerfilComponent,
    CanvasLabComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    StudentRoutingModule,

    NgxPaginationModule,
    Ng2SearchPipeModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    CanvasWhiteboardModule


  ]
})
export class StudentModule { }
