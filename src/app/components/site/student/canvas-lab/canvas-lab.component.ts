import { Location } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { roundToNearestMinutesWithOptions } from 'date-fns/fp';
import Konva from 'konva';
import { CryptoService } from 'src/app/services/crypto.service';
import { LaboratoriosService } from 'src/app/services/laboratorios.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-canvas-lab',
  templateUrl: './canvas-lab.component.html',
  styleUrls: ['./canvas-lab.component.scss']
})
export class CanvasLabComponent implements OnInit {

  stage: Konva.Stage;
  layer: Konva.Layer;

  // @ViewChild('canvas', { static: true }) canvas: ElementRef<HTMLCanvasElement>;  
  @ViewChild('canvaContainer', { static: true }) canvaContainer: ElementRef<HTMLCanvasElement>;  
  @ViewChild('canvaTool', { static: true }) canvaTool: ElementRef<HTMLCanvasElement>;  
  @ViewChild('main', { static: true }) main: ElementRef<HTMLCanvasElement>;  
  
  imageName = "../../../../../assets/img/router.png";

  lab;

  nodes:any;
  links:any;
  node:any={};

  ports:any=[];

  private color = 'red';
  private x = 0;
  private y = 0;
  private z = 30;

  flag=false;

  link:any={
    nodes:[]
  }

  linkdel={};

  constructor(private crypt:CryptoService,
    private router:Router,
    private localstorage:LocalStorageService,
    private location:Location,
    private labService:LaboratoriosService) { 
      
      
    }

    resetLink(){
      this.link={
        nodes:[]
      }
    }

  ngOnInit(): void {
    //canvas margin top
    var nav=document.getElementById("navbar-lab");
    var clientHeight = nav.clientHeight;
    this.canvaContainer.nativeElement.style.paddingTop=""+clientHeight+"px";
    
    //Canvatool
    var clientHeight2 = this.canvaTool.nativeElement.getBoundingClientRect();
    
    //Medidas canvas
    clientHeight2.top-(clientHeight2.bottom-clientHeight2.top);
    
    this.stage = new Konva.Stage({
      container: 'container',
      width: this.canvaContainer.nativeElement.offsetWidth,
      height: clientHeight2.top-(clientHeight2.bottom-clientHeight2.top)
    });

    this.lab = this.crypt.decryptOther();
    this.getLab();
  }

  getLab(){
    var rt=this.lab.lab_users[0].proyect_id;
    
      let router={
        proyect_id:rt.replace(' ', '')
      }
      this.labService.getLabData(router.proyect_id).subscribe(
        (data:any)=>{
          this.nodes=JSON.parse(data.nodes);
          this.links=JSON.parse(data.links);
          this.stage.destroyChildren();
          this.getNodes();
          this.getLinks();
        }
      )
    
  }

  isCanva() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if(titlee.charAt(0) === '#'){
        titlee = titlee.slice( 1 );
    }
    if( titlee === '/student/canvaLab' ) {
        return true;
    }
    else {
        return false;
    }
  }

  return(){
    this.router.navigateByUrl('/student/laboratorios').then(
      ()=>{
        window.location.reload();
        this.localstorage.remove('foo2');
      }
    );
  }

  addRouterc7200(){
    var clientHeight2 = this.canvaTool.nativeElement.getBoundingClientRect();

    var centerX=this.canvaContainer.nativeElement.offsetWidth/2;
    var centerY=(clientHeight2.top-(clientHeight2.bottom-clientHeight2.top))/2;
    var rt=this.lab.lab_users[0].proyect_id;
    var rtr = prompt("Ingresar nombre de router:", "");
    if (rtr == null || rtr == "") {
    } else {
      let router={
        proyect_id:rt.replace(' ', ''),
        name:rtr
      }
      this.labService.addc7200(router).subscribe(
        (data:any)=>{
          this.insertNode(centerX+data.x,centerY+data.y,data.name,data.node_id,data);
        }
      )
    }
  }

  getNodes(){
    var clientHeight2 = this.canvaTool.nativeElement.getBoundingClientRect();

    var centerX=this.canvaContainer.nativeElement.offsetWidth/2;
    var centerY=(clientHeight2.top-(clientHeight2.bottom-clientHeight2.top))/2;    
    
    var rt=this.lab.lab_users[0].proyect_id;
    let router={
      proyect_id:rt.replace(' ', '')
    }

    this.nodes.forEach(element => {
                
      this.insertNode(centerX+element.x,centerY+element.y,element.name,element.node_id,element);
    });
  }

  getLinks(){
    var rt=this.lab.lab_users[0].proyect_id;
    let router={
      proyect_id:rt.replace(' ', '')
    }     
    this.links.forEach(element => {
        var nodes=element.nodes;
        this.insertLink(nodes[0].node_id,nodes[1].node_id,element.link_id,element.project_id);
    });
  }

  insertLink(id1,id2,linkId,project_id){
    var that=this;
    var layer = new Konva.Layer();
    
    var id='#'+id1;
    var idd='#'+id2;
    var routerStart=this.stage.find(id);
    var routerEnd=this.stage.find(idd);

    // dashed line
    var link = new Konva.Line({
      points: [(routerStart[0].attrs.datax+40)
      , (routerStart[0].attrs.datay+40)
      , (routerEnd[0].attrs.datax+40), 
      (routerEnd[0].attrs.datay+40)],
      stroke: 'black',
      strokeWidth: 2,
      lineJoin: 'round',
      linkId:linkId,
      /*
      * line segments with a length of 33px
      * with a gap of 10px
      */
      dash: [33, 10],
    });
    link.on('mouseover', function (evt) {
      // var shape = evt.target;
      evt.target.setAttr('stroke','red');
      evt.target.draw();
      layer.draw();
    });
    layer.on('click', function (evt) {
      var menuNode = document.getElementById('menu2');
      
      if(menuNode.style.display == 'none'){
        // show menu
        menuNode.style.display = 'initial';
        var containerRect = that.stage.container().getBoundingClientRect();
        menuNode.style.top =
          containerRect.top + that.stage.getPointerPosition().y + 4 + 'px';
        menuNode.style.left =
          containerRect.left + that.stage.getPointerPosition().x + 4 + 'px';
          
        that.linkdel['project_id']=project_id;
        that.linkdel['link_id']=linkId;
      }
      else{
        menuNode.style.display = 'none';
      }
      
    });

    link.on('mouseout', function (evt) {
      evt.target.setAttr('stroke','black');
      evt.target.draw();
      layer.draw();
    });

    layer.add(link);
    
    this.stage.add(layer);
  }

  insertNode(x,y,name,id,data){
    
    var clientHeight2 = this.canvaTool.nativeElement.getBoundingClientRect();

    var centerX=this.canvaContainer.nativeElement.offsetWidth/2;
    var centerY=(clientHeight2.top-(clientHeight2.bottom-clientHeight2.top))/2;

    var stage =this.stage;
    
    var imageObj = new Image();
    imageObj.src = this.imageName;
    var wrh = imageObj.width / imageObj.height;
    var newWidth = 80;
    var newHeight = newWidth / wrh;

    //Shape layer
    var shapesLayer = new Konva.Layer({
      id:id,
      name:name,
      data:data,
      datax:x,
      datay:y
    });
    
    var group = new Konva.Group({
      draggable: true
      
    });
    var img = new Konva.Image({
      x: x,
      y: y,
      image: imageObj,
      width: newWidth,
      height: newHeight
    });
    imageObj.onload=function(){
      img.image(imageObj);
      shapesLayer.draw();
    }

    // simple label
    var simpleLabel = new Konva.Label({
      x: x,
      y: y,
      opacity: 0.75,
    });

    simpleLabel.add(
      new Konva.Text({
        text: name,
        fontFamily: 'TypeWriter',
        fontSize: 15,
        padding: 5,
        fill: 'black',
      })
    );
    group.add(img);
    group.add(simpleLabel);
    var that = this;
    group.on('dragend', function (evt) {
        var rt=that.lab.lab_users[0].proyect_id;
        var shape = evt.target;
        var newPos=stage.getPointerPosition();
        var newX=newPos.x;
        var newY=newPos.y;
        newPos.x=newPos.x-centerX;
        newPos.y=newPos.y-centerY;
        
        that.labService.changeDevicePos(rt.replace(' ', ''),id,newPos).subscribe(
          (data:any)=>{
            that.getLab();
          }
        )
    });

    group.on('click', function () {
      if(that.stage.container().style.cursor=='crosshair'){
        var menuNode = document.getElementById('menu');
        
        // show menu
        menuNode.style.display = 'initial';
        var containerRect = stage.container().getBoundingClientRect();
        menuNode.style.top =
          containerRect.top + stage.getPointerPosition().y + 4 + 'px';
        menuNode.style.left =
          containerRect.left + stage.getPointerPosition().x + 4 + 'px';

        that.ports=data
      }
      else{
        var menuNode = document.getElementById('menu3');
        
        // show menu
        menuNode.style.display = 'initial';
        var containerRect = stage.container().getBoundingClientRect();
        menuNode.style.top =
          containerRect.top + stage.getPointerPosition().y + 4 + 'px';
        menuNode.style.left =
          containerRect.left + stage.getPointerPosition().x + 4 + 'px';

          that.node['data']=data;
          
      }
    });
    

    shapesLayer.add(group);
    this.stage.add(shapesLayer);
    
  }

  linkNodes(){
    if(this.stage.container().style.cursor=='default'){
      this.stage.container().style.cursor = 'crosshair';
    }
    else{
      this.stage.container().style.cursor = 'default';
      var menuNode = document.getElementById('menu');
      menuNode.style.display = 'none';
      this.ports=[];
      this.resetLink();
    }
  }

  getPortUsed(data,n){
    var flag=false;
    
    this.links.forEach(element1 => {
      element1.nodes.forEach(element => {
        if(data.adapter_number==element.adapter_number 
          && data.port_number==element.port_number
          && n.node_id==element.node_id){
          flag=true;
        }
      });
    });
    return flag;
  }

  selectPort(p,node){
    var n:any={
      "adapter_number": p.adapter_number, 
      "node_id": node.node_id, 
      "port_number": p.port_number
    }

    if(this.link.nodes.length<2){
      this.link.nodes.push(n);      
    }
    if(this.link.nodes.length==2){
      this.labService.linkNodes(node.project_id,this.link).subscribe(
        (data:any)=>{
          this.getLab();
          this.resetLink();
          this.ports=[];
          this.stage.container().style.cursor = 'default';
          var menuNode = document.getElementById('menu');
          menuNode.style.display = 'none';
        }
      );
    }

  }

  cancelLink(){
    var menuNode = document.getElementById('menu');
    menuNode.style.display = 'none';
  }

  unLink(){
    var menuNode = document.getElementById('menu2');
    menuNode.style.display = 'none';

    this.labService.unlink(this.linkdel['project_id'],this.linkdel['link_id']).subscribe(
      (data:any)=>{
        this.linkdel={};
        this.getLab();
      }
    )
    
  }

  turnOnNode(){
    this.labService.turnOnNode(this.node.data.project_id,this.node.data.node_id).subscribe(
      (data:any)=>{
        var menuNode = document.getElementById('menu2');
        menuNode.style.display = 'none';
        this.getLab();
        this.node={};
      }
    )
  }

  turnOffNode(){
    this.labService.turnOffNode(this.node.data.project_id,this.node.data.node_id).subscribe(
      (data:any)=>{
        var menuNode = document.getElementById('menu2');
        menuNode.style.display = 'none';
        this.getLab();
        this.node={};
      }
    )
  }

  showConsole(){

  }

  deleteNode(){

  }

  closeNodeMenu(){
    this.node={};
  }


}



