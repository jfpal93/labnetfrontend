import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvasLabComponent } from './canvas-lab.component';

describe('CanvasLabComponent', () => {
  let component: CanvasLabComponent;
  let fixture: ComponentFixture<CanvasLabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanvasLabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanvasLabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
