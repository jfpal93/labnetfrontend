import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PerfilComponent } from './perfil/perfil.component';
import { LaboratoriosComponent } from './laboratorios/laboratorios.component';
import { CalificacionesComponent } from './calificaciones/calificaciones.component';
import { CanvasLabComponent } from './canvas-lab/canvas-lab.component';

const routes: Routes = [
  {
    path:'',
    children:
    [
      {
        path: 'calificaciones', 
        data:{role:'student'},
        component: CalificacionesComponent 
      },
      {
        path: 'laboratorios', 
        data:{role:'student'},
        component: LaboratoriosComponent 
      },
      {
        path: 'perfil', 
        data:{role:'student'},
        component: PerfilComponent 
      },
      {
        path: 'canvaLab', 
        data:{role:'student'},
        component: CanvasLabComponent 
      },
      {
        path: '', redirectTo: '', pathMatch: 'full',
        component: DashboardComponent
      },
      // { path: '**', component:  Client404Component}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
