import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CursosService } from 'src/app/services/cursos.service';
import { ProfesoresService } from 'src/app/services/profesores.service';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {

  public ccls:any[];

  showLayout:boolean=true;

  public est={
    id:null,
    name:null,
    lastname:null,
    email:null,
    telefono:null
  };

  search:string;
  p;
  p2;

  constructor(
    private teacherService:ProfesoresService,
    private auth:AuthService
    ) { }

  ngOnInit(): void {
    this.getCursos();
  }

  getCursos(){
    
    this.teacherService.getCourses(this.auth.getID()).subscribe(
      (data:any)=>{
        this.ccls=data;
      }
    );
  }

  showEstudiante(e){
    this.est=e;
    document.getElementById("openModalButton").click();
  }

  close(){
    document.getElementById("closeModalButton").click();
  }

}
