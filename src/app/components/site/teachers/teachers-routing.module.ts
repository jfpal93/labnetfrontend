import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PerfilComponent } from './perfil/perfil.component';
import { LaboratoriosComponent } from './laboratorios/laboratorios.component';
import { CursosComponent } from './cursos/cursos.component';
import { CalificacionesComponent } from './calificaciones/calificaciones.component';

const routes: Routes = [
  {
    path:'',
    children:
    [
      {
        path: 'calificaciones', 
        data:{role:'teachers'},
        component: CalificacionesComponent
      },
      {
        path: 'cursos', 
        data:{role:'teachers'},
        component: CursosComponent
      },
      {
        path: 'laboratorios', 
        data:{role:'teachers'},
        component: LaboratoriosComponent 
      },
      {
        path: 'perfil', 
        data:{role:'teachers'},
        component: PerfilComponent
      },
      {
        path: '', redirectTo: '', pathMatch: 'full',
        component: DashboardComponent
      },
      // { path: '**', component:  Client404Component}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeachersRoutingModule { }
