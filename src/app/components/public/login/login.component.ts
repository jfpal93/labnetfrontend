import { Component, OnInit } from '@angular/core';
import { JWTTokenService } from 'src/app/services/jwttoken.service';
import { AuthService } from './../../../services/auth.service';
import { LocalStorageService } from './../../../services/local-storage.service';
import { CryptoService } from './../../../services/crypto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form = {
    email: null,
    password: null
  };

  fieldTextType: boolean;
  errorLogin:boolean=false;
  loginEmailValid:boolean=true;
  loadingLogin:boolean=false;
  servererr:boolean=false;


  constructor(
    private authService:AuthService,
    private localStorage:LocalStorageService,
    private encryptService:CryptoService,
    private router:Router,
    private jwt:JWTTokenService
  ) { }

  ngOnInit(): void {
  }

  async onSubmit() {
    this.servererr=false;
    this.loadingLogin=true;
    this.errorLogin=false;
    let data;
    this.authService.login(this.form).subscribe(
      (data:any) => {
        this.handelResponse(data);
      },
      (error:any) => {
        this.handleError(error)
      }
    );
    
  }

  handelResponse(data){
    this.loadingLogin=false;
    
    if(data.error == 401){
      this.errorLogin=true;
    }

    this.encryptService.encrypt(data);

    this.router.navigateByUrl('/'+data.user.roles[0].name);
  }

  handleError(error)
  {
    this.loadingLogin=false;
    if(error.error == 401){
      this.errorLogin=true;
    }
    else{
      this.servererr=true;
    }
    
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

  validateEmailLoginForm(event: any)
  {
    if(event.target.value != ""){
      var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if(event.target.value.match(mailformat))
      {      
        this.loginEmailValid= true;
      }
      else
      {
        this.loginEmailValid= false;
      }
    }
  }

}
